<?php

namespace App\Admin\Controllers;

use App\Models\Post;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Category;

class PostController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '文章列表';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Post());
        $grid->id('ID')->sortable();
        $grid->title('标题');
        $grid->is_show('显示')->display(function ($value) {
            return $value ? '是' : '否';
        });
        $grid->like_count('点赞数');
        $grid->favorite_count('收藏数');
        $grid->created_at('创建时间')->display(function ($value) {
            return date('Y-m-d H:i:s', strtotime($value));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Post::findOrFail($id));



        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $this->title = '创建文章';
        $form = new Form(new Post());
        $form->text('title', '文章标题')->rules('required');
        $form->select('cat_id', '文章分类')
            ->options(Category::pluck('cat_name', 'id'));

        $form->simplemde('content', '文章内容')->rules('required');
        $form->text('description', '文章描述');
        $form->text('slug', 'Slug');
        $form->text('keyword', '关键词');
        $form->image('thumb', '封面图片')->rules('image');
        $form->radio('is_show', '显示')->options(['1' => '是', '0' => '否'])->default('0');


        return $form;
    }
}
