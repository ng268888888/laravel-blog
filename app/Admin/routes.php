<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->get('users', 'UserController@index');
    $router->get('posts', 'PostController@index');

    $router->get('posts/create', 'PostController@create');
    $router->post('posts', 'PostController@store');

    $router->get('posts/{id}/edit', 'PostController@edit');
    $router->put('posts/{id}', 'PostController@update');

    $router->get('categories', 'CateController@index');
    $router->get('categories/create', 'CateController@create');
    $router->post('categories', 'CateController@store');
    $router->get('categories/{id}/edit', 'CateController@edit');
    $router->put('categories/{id}', 'CateController@update');

});
