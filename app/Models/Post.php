<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['cat_id', 'title', 'description', 'content', 'slug', 'keyword', 'thumb', 'is_show', 'like_count', 'favorite_count'];

    protected $casts = ['is_show' => 'boolean'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
