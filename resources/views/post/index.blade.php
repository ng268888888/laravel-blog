@extends('layouts.app')
@section('title', '文章列表')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9">
      @foreach($posts as $post)
      <div class="d-md-flex post-entry-2 small-img">
        <a href="#" class="me-4 thumbnail">
          <img src="{{ $post->thumb }}" alt="" class="img-fluid">
        </a>
        <div>
          <div class="post-meta"><span class="date">{{ $post->cateogry->cat_name }}</span> <span class="mx-1">•</span> <span>Jul 5th '22</span></div>
          <h3><a href="single-post.html">What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</a></h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio placeat exercitationem magni voluptates dolore. Tenetur fugiat voluptates quas.</p>
        </div>
      </div>
      @endforeach
      <!-- Paging -->
      <div class="text-start py-4">
        <div class="custom-pagination">
          <a href="#" class="prev">Prevous</a>
          <a href="#" class="active">1</a>
          <a href="#">2</a>
          <a href="#">3</a>
          <a href="#">4</a>
          <a href="#">5</a>
          <a href="#" class="next">Next</a>
        </div>
      </div><!-- End Paging -->
    </div>
    <div class="col-md-3">
  <!-- ======= Sidebar ======= -->
  @include('layouts.sidebar')
<!-- End Tags -->
</div>  
</div>
</div>
@stop