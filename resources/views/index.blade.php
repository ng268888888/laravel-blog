@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="d-md-flex post-entry-2 small-img">
        <a href="single-post.html" class="me-4 thumbnail">
          <img src="assets/img/post-landscape-1.jpg" alt="" class="img-fluid">
        </a>
        <div>
          <div class="post-meta"><span class="date">Business</span> <span class="mx-1">&bullet;</span> <span>Jul 5th '22</span></div>
          <h3><a href="single-post.html">What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</a></h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio placeat exercitationem magni voluptates dolore. Tenetur fugiat voluptates quas.</p>
        </div>
      </div>
      <div class="d-md-flex post-entry-2 small-img">
        <a href="single-post.html" class="me-4 thumbnail">
          <img src="assets/img/post-landscape-1.jpg" alt="" class="img-fluid">
        </a>
        <div>
          <div class="post-meta"><span class="date">Business</span> <span class="mx-1">&bullet;</span> <span>Jul 5th '22</span></div>
          <h3><a href="single-post.html">What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</a></h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio placeat exercitationem magni voluptates dolore. Tenetur fugiat voluptates quas.</p>
        </div>
      </div>
      <div class="d-md-flex post-entry-2 small-img">
        <a href="single-post.html" class="me-4 thumbnail">
          <img src="assets/img/post-landscape-1.jpg" alt="" class="img-fluid">
        </a>
        <div>
          <div class="post-meta"><span class="date">Business</span> <span class="mx-1">&bullet;</span> <span>Jul 5th '22</span></div>
          <h3><a href="single-post.html">What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</a></h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio placeat exercitationem magni voluptates dolore. Tenetur fugiat voluptates quas.</p>
        </div>
      </div>


      <!-- Paging -->
      <div class="text-start py-4">
        <div class="custom-pagination">
          <a href="#" class="prev">Prevous</a>
          <a href="#" class="active">1</a>
          <a href="#">2</a>
          <a href="#">3</a>
          <a href="#">4</a>
          <a href="#">5</a>
          <a href="#" class="next">Next</a>
        </div>
      </div><!-- End Paging -->
    </div>
    @include('layouts.sidebar')
  </div>
</div>
@stop